from bonus_system import calculateBonuses

STANDARD = "Standard"
PREMIUM = "Premium"
DIAMOND = "Diamond"

def testStandartProgram():
    assert calculateBonuses(STANDARD, 1) == 0.5 * 1
    assert calculateBonuses(STANDARD, 10000) == 0.5 * 1.5
    assert calculateBonuses(STANDARD, 10001) == 0.5 * 1.5
    assert calculateBonuses(STANDARD, 50000) == 0.5 * 2
    assert calculateBonuses(STANDARD, 50001) == 0.5 * 2
    assert calculateBonuses(STANDARD, 100000) == 0.5 * 2.5
    assert calculateBonuses(STANDARD, 100001) == 0.5 * 2.5


def testPremiumProgram():
    assert calculateBonuses(PREMIUM, 1) == 0.1 * 1
    assert calculateBonuses(PREMIUM, 10000) == 0.1 * 1.5
    assert calculateBonuses(PREMIUM, 10001) == 0.1 * 1.5
    assert calculateBonuses(PREMIUM, 50000) == 0.1 * 2
    assert calculateBonuses(PREMIUM, 50001) == 0.1 * 2
    assert calculateBonuses(PREMIUM, 100000) == 0.1 * 2.5
    assert calculateBonuses(PREMIUM, 100001) == 0.1 * 2.5

def testDiamondProgram():
    assert calculateBonuses(DIAMOND, 1) == 0.2 * 1
    assert calculateBonuses(DIAMOND, 10000) == 0.2 * 1.5
    assert calculateBonuses(DIAMOND, 10001) == 0.2 * 1.5
    assert calculateBonuses(DIAMOND, 50000) == 0.2 * 2
    assert calculateBonuses(DIAMOND, 50001) == 0.2 * 2
    assert calculateBonuses(DIAMOND, 100000) == 0.2 * 2.5
    assert calculateBonuses(DIAMOND, 100001) == 0.2 * 2.5


def testIncorrectProgram():
    assert calculateBonuses("Standar", 1) == 0
    assert calculateBonuses("Diamon", 1) == 0
    assert calculateBonuses("Premiu", 1) == 0
    assert calculateBonuses("Standardd", 1) == 0
    assert calculateBonuses("Diamondd", 1) == 0
    assert calculateBonuses("Premiumm", 1) == 0
    assert calculateBonuses("", 1) == 0
